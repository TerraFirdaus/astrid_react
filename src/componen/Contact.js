import React from 'react'
import '../assets/css/style.css'
// import { Container} from 'react-bootstrap'
import whatsapp from '../assets/images/whatsapp.png'
import mail from '../assets/images/mail.svg'

class Contact extends React.Component {
    render() {
        return(
            <div>
                <div className="Rectangle-26">
                    <div className="orange-bar"></div>
                    <span className="greatings1">Contact Service Desk</span>
                    <p className="greating2">Home - Contact Service Desk</p>
                </div>

                <div className="quick">
                    <a md = "3" lg = "3" className="Rectangle-9" href="/home">
                        <div className="orange-bar"></div>
                        <div className="kantong">
                            <span className="quickmenu1">WhatsApp</span>
                            <br/>
                            <span className="quickmenu1">+62878-7291-8333</span>
                        </div>
                        <img src={ whatsapp } alt="whatsapp img" className="whatsapp"/>
                    </a>
                    <div md = "3" lg = "3" className="Rectangle-9" href="https://googl.com">
                        <div className="orange-bar"></div>
                        <div className="kantong">
                            <span className="quickmenu1">Email</span>
                            <br/>
                            <span className="quickmenu1">service.desk@kbbukopin.com</span>
                        </div>
                        <img src={ mail } alt="mail img" className="mail"/>
                    </div>
                </div>
            </div>
        )
    }
}

export default Contact