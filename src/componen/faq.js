import React from 'react'
import '../assets/css/style.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Accordion } from 'react-bootstrap'

class Faq extends React.Component {
    render() {
        return(
            <div>
                <div className="Rectangle-26">
                    <div className="orange-bar"></div>
                    <span className="greatings1">Release User Swamitra</span>
                    <p className="greating2">Home - Direct Service - Release User Swamitra</p>
                </div>

                <div className="req1">
                <div className="orange-bar"></div>
                    <div className="question">
                        <Accordion defaultActiveKey="0">
                            <Accordion.Item eventKey="0" className="accordion-info">
                                <Accordion.Header className="question0">
                                    <strong>
                                        Apa Itu ASTRID Helpdesk?
                                    </strong>
                                </Accordion.Header>
                                <Accordion.Body>
                                    <p>Astrid Helpdesk merupakan sebuah web internal yang berfungsi untuk membantu pelayanan para team 
                                    di seluruh indonesia untuk mendapatkan pelayanan yang cepat dan prima yang berhubungan 
                                    dengan pelayanan service desk</p>
                                </Accordion.Body>
                            </Accordion.Item>

                            <Accordion.Item eventKey="1" className="accordion-info">
                                <Accordion.Header className="question1">
                                    <strong>
                                        Apa saja layanan yang tersedia di Astrid Helpdesk?
                                    </strong>
                                </Accordion.Header>
                                <Accordion.Body>
                                    <p>Astrid Helpdesk merupakan sebuah web internal yang berfungsi untuk membantu pelayanan para team 
                                    di seluruh indonesia untuk mendapatkan pelayanan yang cepat dan prima yang berhubungan 
                                    dengan pelayanan service desk</p>
                                </Accordion.Body>
                            </Accordion.Item>

                            <Accordion.Item eventKey="2" className="accordion-info">
                                <Accordion.Header className="question2">
                                    <strong>
                                    Berapa Lama SLA Untuk layanan ini?
                                    </strong>
                                </Accordion.Header>
                                <Accordion.Body>
                                    <p>Astrid Helpdesk merupakan sebuah web internal yang berfungsi untuk membantu pelayanan para team 
                                    di seluruh indonesia untuk mendapatkan pelayanan yang cepat dan prima yang berhubungan 
                                    dengan pelayanan service desk</p>
                                </Accordion.Body>
                            </Accordion.Item>

                            <Accordion.Item eventKey="3" className="accordion-info">
                                <Accordion.Header className="question3">
                                    <strong>
                                    Apakah saya akan mendapatkan bukti bahwa saya telah mengajukan permintaan?
                                    </strong>
                                </Accordion.Header>
                                <Accordion.Body>
                                    <p>Astrid Helpdesk merupakan sebuah web internal yang berfungsi untuk membantu pelayanan para team 
                                    di seluruh indonesia untuk mendapatkan pelayanan yang cepat dan prima yang berhubungan 
                                    dengan pelayanan service desk</p>
                                </Accordion.Body>
                            </Accordion.Item>

                            <Accordion.Item eventKey="4" className="accordion-info">
                                <Accordion.Header className="question4">
                                    <strong>
                                    Jika layanan sudah melebihi SLA, saya harus menghubungi kemana?
                                    </strong>
                                </Accordion.Header>
                                <Accordion.Body>
                                    <p>Astrid Helpdesk merupakan sebuah web internal yang berfungsi untuk membantu pelayanan para team 
                                    di seluruh indonesia untuk mendapatkan pelayanan yang cepat dan prima yang berhubungan 
                                    dengan pelayanan service desk</p>
                                </Accordion.Body>
                            </Accordion.Item>
                        </Accordion>

                    </div>
                    
                </div>
            </div>
        )
    }
}

export default Faq