import React from 'react'
import '../assets/css/style.css'
import background1 from '../assets/images/BG_Login.png'
import logo1 from '../assets/images/Logo_KB_Big.png'
import astrid1 from '../assets/images/astrid2.jpg'
import { Container, Row, Col } from 'react-bootstrap'

class login extends React.Component {
    render() {
        return(
            <Container id = "loginpage">
                <Row className="banner">
                    <div className="wrapper-banner" style={{backgroundImage: `url(' ${ background1 }')`}}>
                        <Col md = "6" lg = "6" className="wrapper-text-banner"> 
                            <img src={ logo1 } alt="gambar pertama" className="logologin" />
                        </Col>
                        <Col md = "3" lg = "6" className="Rectangle-12 card">
                            <img src={ astrid1 } alt="astrid"className="astrid-11"/>
                            <span className="welcome">A S T R I D</span>
                            <form>
                                <div className="form-group1">
                                    <input type="text" className="form-control" id="InputUser" placeholder="User ID" />
                                </div>
                                <div className="form-group2">
                                    <input type="password" className="form-control" id="InputPassword" placeholder="Password" />
                                </div>
                                <a className="tombol" href="/home">LOGIN</a>
                                <br/>
                                <br/>
                                <a className="Forgot-Password" href="/home">Forgot Password</a>
                            </form>
                        </Col>
                    </div>
                </Row>
            </Container>
        )
    }
}

export default login
