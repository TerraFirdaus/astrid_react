import React from 'react'
import '../assets/css/style.css'
import { Container, Row, Col } from 'react-bootstrap'
import background2 from '../assets/images/Background.png'
import Sidebar from './Sidebar'
import Recent from './Recent'
import Quickmenu from './Quickmenu'
// import ReqSD from './ReqSD'
// import Contact from './Contact'
// import Direct from './Direct'

class Home extends React.Component {
    render() {
        return(
            <Container className="container-fluid" id="sidebar" style={{backgroundImage: `url(' ${ background2 }')`}}>
                <Row style={{display:"flex"}}>
                    <Col md = "3" lg = "3" >
                        <Sidebar />
                    </Col>
                    <Col md = "6" lg = "6">
                        <Quickmenu />
                    </Col>
                    <Col md= "3" lg = "6">
                        <Recent />
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default Home