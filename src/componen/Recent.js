import React from 'react'
import '../assets/css/style.css'
// import { Container} from 'react-bootstrap'
import userimg from '../assets/images/girlphoto.jpg'

class Recent extends React.Component {
    render() {
        return(
            <div>
                <div className="Rectangle-27">
                    <img src={ userimg } alt="user img" className="photo1"/>
                    <span className="userName">Help Desk User Name</span>
                    <p className="email">email@kbbukopin.com</p>
                </div>

                <div className="Rectangle-7">
                    <span className="My-Recent-Activities">My Recent Activities</span>
                    <div className="Path-15"></div>
                    <p className="recent-tgl">10 Nov 2021 08:15</p>
                    <span className="Release-User">Release User xxx.xxxx</span>
                </div>
            </div>
        )
    }
}

export default Recent