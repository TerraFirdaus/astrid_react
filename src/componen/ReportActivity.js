import React from 'react'
import '../assets/css/style.css'
import 'bootstrap/dist/css/bootstrap.min.css'
// import { Container} from 'react-bootstrap'

class ReportActivity extends React.Component {
    render() {
        return(
            <div>
                <div className="Rectangle-26">
                    <div className="orange-bar"></div>
                    <span className="greatings1">Report Activity</span>
                    <p className="greating2">Home - Report Activity</p>
                </div>

                <div className="req">
                    <div md="3" lg = "3" className="Bar">
                    <span className="text4">User ID</span>
                    <input type="text" id="isian2" name="isian2" placeholder="User ID"></input>
                    </div>
                    <div md="3" lg = "3" className="Bar">
                    <span className="text4">Tanggal</span>
                    <div className="text5">11 Nov 2021</div>
                    <div className="text6">-</div>
                    <div className="text5">11 Nov 2021</div>
                    </div>

                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">Time</th>
                                <th scope="col">Date</th>
                                <th scope="col">User ID</th>
                                <th scope="col">Request</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>17:33</td>
                                <td>12/11/2021</td>
                                <td>SDM010101</td>
                                <td>Release User</td>
                                <td>Sukses</td>
                            </tr>
                            <tr>
                            <th scope="row">2</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                            <th scope="row">3</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>

                    <a className="tombol2" href="/home">Retrieve</a>
                    <a className="tombol2" href="/home">Save To Excel</a>
                </div>
            </div>
        )
    }
}

export default ReportActivity