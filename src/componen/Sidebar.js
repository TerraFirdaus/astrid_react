import React from 'react'
import '../assets/css/style.css'
// import { Container} from 'react-bootstrap'
import logo1 from '../assets/images/KB_Bukopin.svg'
import homeimg from '../assets/images/home.svg'
import usercheckimg from '../assets/images/user-check.svg'
import reqsdimg from '../assets/images/mail.svg'
import chatimg from '../assets/images/message-circle.svg'
import contactsdimg from '../assets/images/phone-call.svg'
import userguideimg from '../assets/images/book-open.svg'
import reportactimg from '../assets/images/inbox.svg'
import faqimg from '../assets/images/help-circle.svg'
import logoutimg from '../assets/images/log-out.svg'

class Sidebar extends React.Component {
    render() {
        return(
            <div>
            <div className="Rectangle-5">
                <img src={ logo1 } alt="gambar pertama" className="Image-1" />
                <div className="Menu">
                    <ul className="wrapper-list-menu">
                        <li className="list-menu">
                            <div className="box-left"></div>
                            <img src={ homeimg } alt="home img" className="iconsidebar"/>
                            <a className="linksidebar" href="/home">Home</a>
                        </li>
                        <br /><br />
                        <li className="list-menu">
                            <img src={ usercheckimg } alt="direct service img" className="iconsidebar"/>
                            <a className="linksidebar" href="/direct">Direct Services</a>
                        </li>
                        <br /><br />
                        <li className="list-menu">
                            <img src={ reqsdimg } alt="req to sd img" className="iconsidebar"/>
                            <a className="linksidebar" href="/reqsd">Request To Service Desk</a>
                        </li>
                        <br /><br />
                        <li className="list-menu">
                            <img src={ chatimg } alt="chat astrid img" className="iconsidebar"/>
                            <a className="linksidebar" href="/home">Chat With Astrid</a>
                        </li>
                        <br /><br />
                        <li className="list-menu">
                            <img src={ contactsdimg } alt="contact sd img" className="iconsidebar"/>
                            <a className="linksidebar" href="/contact">Contact Service Desk</a>
                        </li>
                        <br /><br />
                        <li className="list-menu">
                            <img src={ userguideimg } alt="User Guide img" className="iconsidebar"/>
                            <a className="linksidebar" href="/home">User Guide</a>
                        </li>
                        <br /><br />
                        <li className="list-menu">
                            <img src={ reportactimg } alt="report act img" className="iconsidebar"/>
                            <a className="linksidebar" href="/reportactvity">Report Activity</a>
                        </li>
                        <br /><br />
                        <li className="list-menu">
                            <img src={ faqimg } alt="FAQ img" className="iconsidebar"/>
                            <a className="linksidebar" href="/faq">FAQ</a>
                        </li>
                        <br /><br />
                        <li className="list-menu">
                            <img src={ logoutimg } alt="log out img" className="iconsidebar"/>
                            <a className="linksidebar" href="/">Log Out</a>
                        </li>
                    </ul>
                </div>
            </div>
            </div>
        )
    }
}

export default Sidebar