import React from 'react'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
// import {
//     BrowserRouter as Router,
//     Switch,
//     Route,
//   } from "react-router-dom";

import Login from './componen/login'
import Home from './componen/Home'
import Bukidesk from './componen/Bukidesk'
import Bukisys from './componen/Bukisys'
import Swamitra from './componen/Swamitra'
import Cls from './componen/Cls'
import Silverlake from './componen/Silverlake'
import ReDisplay from './componen/ReDisplay'
import ReportActivity from './componen/ReportActivity'
import Contact from './componen/Contact'
import ReqSD from './componen/ReqSD'
import Direct from './componen/Direct'
import Faq from './componen/Faq'

class Routers extends React.Component {
    render () {
        return(
            <BrowserRouter>
                <Routes>
                    <Route exact path="/" element= { <Login /> } />            
                    <Route exact path="*" element={ <Navigate replace to="/" /> } />
                    <Route exact path="/home" element= { <Home /> } />
                    <Route exact path="/bukidesk" element= { <Bukidesk />} />
                    <Route exact path="/bukisys" element= { <Bukisys />} />
                    <Route exact path="/swamitra" element= { <Swamitra />} />
                    <Route exact path="/cls" element= { <Cls />} />
                    <Route exact path="/silverlake" element= { <Silverlake />} />
                    <Route exact path="/redisplay" element= { <ReDisplay />} />
                    <Route exact path="/reportactvity" element= { <ReportActivity />} />
                    <Route exact path="/contact" element= { <Contact />} />
                    <Route exact path="/reqsd" element= { <ReqSD />} />
                    <Route exact path="/direct" element= { <Direct />} />
                    <Route exact path="/faq" element= { <Faq />} />
                </Routes>      
          </BrowserRouter>
        )
    }
}

export default Routers